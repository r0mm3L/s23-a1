
// CRUD
// Create, Read, Update and Delete

/* 


CREATE


*/
	// allows us to create documents under a collection or create a collection if it does not exist yet.

	// Syntax
	// db.collections.insertOne()
		// allows to insert or create one document.
		// collections- stands for collection name

	db.users.insertOne(
		{
			"firstName": "Tony",
			"lastName" : "Stark",
			"username" : "iAmIronMan",
			"email" : "iloveyou3000@mail.com",
			"password" : "starkIndustries",
			"isAdmin" : true
		}
	)

	// db.collections.insertMany([{}, {}])
		// allows us to insert or create two or more documents.

	db.users.insertMany([
		{
			"firstName": "Pepper",
			"lastName" : "Potts",
			"username" : "rescueArmor",
			"email" : "pepper@mail.com",
			"password" : "whereIsTonyAgain",
			"isAdmin" : false
		},
		{
			"firstName": "Steve",
			"lastName" : "Rogers",
			"username" : "theCaptain",
			"email" : "captAmerica@mail.com",
			"password" : "iCanLiftMjolnirToo",
			"isAdmin" : false
		},
		{
			"firstName": "Thor",
			"lastName" : "Odinson",
			"username" : "mightyThor",
			"email" : "ThorNotLoki@mail.com",
			"password" : "iAmWorthyToo",
			"isAdmin" : false
		},
		{
			"firstName": "Loki",
			"lastName" : "Odinson",
			"username" : "godOfMischief",
			"email" : "loki@mail.com",
			"password" : "iAmReallyLoki",
			"isAdmin" : false
		},
	])

// mini-activity
	// 1. Make a new collection with the name courses
	// 2. Insert the following fields and values:
		/*
			name: Javascript,
			price: 3500,
			description: Learn Javascript in a week!,
			isActive: true

			name: HTML,
			price: 1000,
			description: Learn Basic HTML in 3 days!,
			isActive: true

			name: CSS,
			price: 2000,
			description: Make you website fancy, learn CSS now!,
			isActive: true
	
		*/

	db.courses.insertMany([
		{
			"name": "Javascript",
			"price": 3500,
			"description": "Learn Javascript in a week!",
			"isActive": true
		},
		{
			"name": "HTML",
			"price": 1000,
			"description": "Learn Basic HTML in 3 days!",
			"isActive": true
		},
		{
			"name": "CSS",
			"price": 2000,
			"description": "Make you website fancy, learn CSS now!",
			"isActive": true
		}
	])

	/*  


	READ



	*/
		// allows us to retrieve data
		// it needs a query or filters to specify the document we are retrieving.

	// Syntax:
		// db.collections.find()
			// allows us to retrieve ALL documents in the collection.

	db.users.find();

	// Syntax:
		// db.collections.findOne({criteria: value})
			// allows us to find the document that matches our criteria

	db.users.find({"isAdmin" : false});


	// no criteria
	// syntax:
		// db.collection.find({}) - allows to find all
		// db.collection.findOne({}) - allows us to find the first document
		db.users.find({})


//syntax:
	// db.collections.find({,})
			//allows us to find the document that satisfy all criteria. 

			db.users.find({"lastName": "odinson", "firstName": "loki"})



	/* 


	UPDATE


	*/


	//allows to update documents.
	// also use criteria or filter
	// $set operator
	// reminder: updates are permanent and can't be rolled back. 

	//syntax:
	  // db.collections.updateOne({}, {$set: {}})
	      // allows us to update one document that satisfy the criteria


	      db.users.updateOne({"lastName": "Potts"}, {$set: {"lastName": "Stark"}})



	  // UPDATE MANY
	  	//syntax
	     // db.collections.updateMany({criteria: value}, {$set: {criteria: updatevalue}})
	     // allows us to update all document that satisfy the criteria 

	     db.users.updateMany({"lastName": "Odinson"}, {$set: {"isAdmin": true}})

	     db.users.updateMany({"lastName": "Stark"}, {$set: {"isAdmin": false}})

	     //syntax:

	     	// db.collections.updateOne({}, {$set: {fieldToBeUpdated: updated}})
	     			// allows us to update the first item in the collection.


	     	db.users.updateOne({}, {$set: {"email": "starkindustries@email.com"}})


	 // mini-activity

	 db.courses.updateOne({"name": "Javascript"}, {$set: {"isActive": false}})

	 db.users.updateMany({}, {$set: {"enrolees": 10}})



/*

DELETE

*/

// allows us to delete documents.
// provide criteria or filters to specify which document to delete from the collection.
// REMINDER: be careful when deleting documents, because it will be complicated to retreive them back again.

// delete one
// syntax: 
	// db.collections.deleteOne({criteria: value})
		// allows us to delete the first item that matches our criteria

		db.users.deleteOne({"isAdmin": false})


// delete many
	// syntax:
	// db.collections.deleteMany({criteria: value})
	// allows us to delete all items that matches our criteria

	db.users.deleteMany({"lastName": "Odinson"})


// delete without criteria

// syntax:
	// db.collections.deleteOne({})
	// allows us to delete the first document

	db.users.deleteOne({})

	// delete many without criteria
	// syntax:
		// db.collection.deleteMany({})
		// allows us to delete all items in the collection.

	db.users.deleteMany({})

















